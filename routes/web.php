<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    $autoparks = App\Autopark::all();
    if(isset(Auth::user()->id))
        $cars = DB::table('cars')->where('driver_id', Auth::user()->id)->get();
    else
        $cars = null;
    return view('start', [
       'autoparks' => $autoparks,
        'cars' => $cars
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create_autopark', 'AutoparkController@createAutoparkIndex');

Route::get('create_car', 'CarController@createCar');

Route::post('/autopark', 'AutoparkController@createAutopark');

Route::post('car', 'CarController@addCar');

Route::delete('autopark/{autopark}', 'AutoparkController@autoparkDelete');

Route::delete('car/{car}', 'CarController@carDelete');

Route::post('edit_autopark/{autopark}', 'AutoparkController@editAutoparkIndex');

Route::post('update_autopark', 'AutoparkController@updateAutopark');

Route::get('/show_autopark/{autopark}', 'AutoparkController@showAutopark');

Route::post('add_car', 'AutoparkController@addCarToAutopark');

Route::post('delete_car', 'AutoparkController@deleteCarFromAutopark');

Route::post('show_car/{car}', 'CarController@showCar');

Route::get('phpinfo', function () {
   phpinfo();
});

Route::get('/test', function() {
   dd(is_id_term('id:1'));
});
