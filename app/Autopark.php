<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Autopark extends Model
{

    public function getCars()
    {
        $cars_id = DB::table('cars__autoparks')->where('autopark_id', $this->id)->get();
        $tmp_cars = [];
        foreach ($cars_id as $id) {
            array_push($tmp_cars, DB::table('cars')->where('id', $id->car_id)->get());
        }
        $cars = [];
        foreach ($tmp_cars as $tmp_car) {
            array_push($cars, $tmp_car[0]);
        }
        return $cars;
    }
}
