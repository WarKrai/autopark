<?php

namespace App\Http\Controllers;

use App\Autopark;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AutoparkController extends Controller
{
    public function createAutoparkIndex()
    {
        return view('create.create_autopark');
    }

    public function createAutopark(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/create_autopark')
                ->withInput()
                ->withErrors($validator);
        }

        $autopark = new Autopark();
        $autopark->name = $request->name;
        $autopark->address = $request->name;
        if (isset($request->schedule)) {
            $autopark->schedule = $request->schedule;
        }
        $autopark->save();

        return redirect('/');
    }

    public function autoparkDelete(Autopark $autopark)
    {
        $autopark->delete();

        return redirect('/');
    }

    public function editAutoparkIndex(Autopark $autopark)
    {
        return view('edit_autopark', [
            'autopark' => $autopark
        ]);
    }

    public function updateAutopark(Request $request)
    {
        DB::table('autoparks')->where('id', $request->id)->update([
            'address' => $request->address,
            'schedule' => $request->schedule
        ]);
        return redirect('/');
    }

    public function showAutopark(Autopark $autopark)
    {
        $cars = $autopark->getCars();
        $added_id = [];
        foreach ($cars as $car) {
            array_push($added_id, $car->id);
        }
        $all_cars = DB::table('cars')->get();
        $otherCars = [];
        foreach ($all_cars as $all_car) {
            $added = false;
            foreach($added_id as $id) {
                if ($all_car->id == $id)
                    $added = true;
            }
            if (!$added) {
                array_push($otherCars, $all_car);
            }
        }
        return view('show.show_autopark', [
            'cars' => $cars,
            'autopark' => $autopark,
            'otherCars' => $otherCars
        ]);
    }

    public function addCarToAutopark(Request $request)
    {
        DB::table('cars__autoparks')->insert([
            'car_id' => $request->car_id,
            'autopark_id' => $request->autopark_id
        ]);

        return redirect('/show_autopark/'.$request->autopark_id);
    }

    public function deleteCarFromAutopark(Request $request)
    {
        DB::table('cars__autoparks')
            ->where('car_id', $request->car_id)
            ->where('autopark_id', $request->autopark_id)
            ->delete();
        return redirect('/show_autopark/'.$request->autopark_id);
    }
}
