<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Car;

class CarController extends Controller
{
    public function createCar()
    {
        return view('create.create_car');
    }

    public function addCar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'car_number' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/create_car')
                ->withInput()
                ->withErrors($validator);
        }

        $car = new Car();
        $car->car_number = $request->car_number;
        $car->driver_id = $request->driver_id;
        $car->save();

        return redirect('/');
    }

    public function carDelete(Car $car)
    {
        $car->delete();

        return redirect('/');
    }

    public function showCar(Car $car)
    {
        return view('show.show_car', [
            'car' => $car,
            'autoparks' => $car->getAutoparks()
        ]);
    }
}
