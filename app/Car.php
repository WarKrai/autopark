<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Car extends Model
{
    public function getAutoparks()
    {
        $autoparks_id = DB::table('cars__autoparks')->where('car_id', $this->id)->get();
        $autoparks = [];
        foreach($autoparks_id as $autopark_id) {
            $autopark = DB::table('autoparks')->where('id', $autopark_id->autopark_id)->get();
            array_push($autoparks, $autopark[0]);
        }
        return $autoparks;
    }
}
