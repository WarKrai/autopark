<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Autopark;
use Faker\Generator as Faker;

$factory->define(Autopark::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'schedule' => $faker->time('H:i:s') . ' - ' . $faker->time('H:i:s'),
    ];
});
