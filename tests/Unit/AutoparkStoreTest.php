<?php

namespace Tests\Unit;

use App\Autopark;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AutoparkStoreTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $autopark = factory(Autopark::class)->create();

        $dbAutopark = Autopark::first();

        $this->assertNotNull($dbAutopark);
    }
}
