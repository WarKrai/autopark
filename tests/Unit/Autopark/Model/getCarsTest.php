<?php

namespace Tests\Unit\Autopark\Model;

use App\Autopark;
use App\Car;
use App\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class getCarsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create([
            'role' => 'driver'
        ]);

        $dbUser = User::first();

        $autopark1 = factory(Autopark::class)->create();

        $autopark2 = factory(Autopark::class)->create();

        $car1 = factory(Car::class)->create([
            'driver_id' => $dbUser->id
        ]);
        $car2 = factory(Car::class)->create([
            'driver_id' => $dbUser->id
        ]);
        $car3 = factory(Car::class)->create([
            'driver_id' => $dbUser->id
        ]);

        DB::table('cars__autoparks')->insert([
            ['autopark_id'=>$autopark1->id, 'car_id'=>$car1->id],
            ['autopark_id'=>$autopark1->id, 'car_id'=>$car2->id],
            ['autopark_id'=>$autopark2->id, 'car_id'=>$car3->id]
        ]);

        $autopark1Obj = Autopark::find($autopark1->id);
        $autopark2Obj = Autopark::find($autopark2->id);

        $autopark1cars = $autopark1Obj->getCars();
        $autopark2cars = $autopark2Obj->getcars();

        $this->assertCount(2, $autopark1cars);
        $this->assertCount(1, $autopark2cars);

        $this->assertTrue($autopark2cars[0]->id === $car3->id);
    }
}
