<?php

namespace Tests\Unit\Car\Model;

use App\Autopark;
use App\Car;
use App\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class getAutoparksTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create([
           'role' => 'driver'
        ]);

        $dbUser = User::first();

        $autopark1 = factory(Autopark::class)->create();

        $autopark2 = factory(Autopark::class)->create();

        $car1 = factory(Car::class)->create([
           'driver_id' => $dbUser->id
        ]);
        $car2 = factory(Car::class)->create([
            'driver_id' => $dbUser->id
        ]);
        $car3 = factory(Car::class)->create([
            'driver_id' => $dbUser->id
        ]);

        DB::table('cars__autoparks')->insert([
           ['autopark_id'=>$autopark1->id, 'car_id'=>$car1->id],
           ['autopark_id'=>$autopark1->id, 'car_id'=>$car2->id],
            ['autopark_id'=>$autopark2->id, 'car_id'=>$car1->id]
        ]);

        $car1Obj = Car::find($car1->id);
        $car2Obj = Car::find($car2->id);
        $car3Obj = Car::find($car3->id);

        $car1autoparks = $car1Obj->getAutoparks();
        $car2autoparks = $car2Obj->getAutoparks();
        $car3autopraks = $car3Obj->getAutoparks();

        $this->assertCount(2, $car1autoparks);
        $this->assertCount(1, $car2autoparks);
        $this->assertCount(0, $car3autopraks);
    }
}
