<?php

namespace Tests\Unit;

use App\Car;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarStoreTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create([
            'role' => 'driver'
        ]);

        $dbUser = User::first();

        $car = new Car();
        $car->car_number = '1111';
        $car->driver_id = $dbUser->id;
        $car->save();

        $dbCar = Car::first();

        $this->assertNotNull($dbCar);
        $this->assertTrue($car->driver_id === $dbUser->id);
    }
}
