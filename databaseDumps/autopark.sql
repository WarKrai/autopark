-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 31 2020 г., 14:27
-- Версия сервера: 5.6.43
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `autopark`
--

-- --------------------------------------------------------

--
-- Структура таблицы `autoparks`
--

CREATE TABLE `autoparks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `autoparks`
--

INSERT INTO `autoparks` (`id`, `name`, `address`, `schedule`, `created_at`, `updated_at`) VALUES
(2, '21313', '21313', NULL, '2020-05-27 09:38:02', '2020-05-27 09:38:02'),
(3, '111111', '111111', '11', '2020-05-28 05:46:30', '2020-05-28 05:46:30'),
(4, 'test', 'test', NULL, '2020-05-28 05:47:31', '2020-05-28 05:47:31'),
(5, 'tst', 'tst', 'sts', '2020-05-28 05:56:55', '2020-05-28 05:56:55');

-- --------------------------------------------------------

--
-- Структура таблицы `cars`
--

CREATE TABLE `cars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `car_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cars`
--

INSERT INTO `cars` (`id`, `car_number`, `driver_id`, `created_at`, `updated_at`) VALUES
(2, '1111', 3, '2020-05-27 11:48:06', '2020-05-27 11:48:06'),
(3, '123123', 3, '2020-05-27 11:48:09', '2020-05-27 11:48:09'),
(5, '123123', 4, '2020-05-27 11:50:10', '2020-05-27 11:50:10'),
(6, '99999', 3, '2020-05-27 16:22:44', '2020-05-27 16:22:44'),
(7, '111111', 3, '2020-05-28 05:44:52', '2020-05-28 05:44:52'),
(8, '11111', 3, '2020-05-28 06:17:53', '2020-05-28 06:17:53'),
(9, 'test_number', 3, '2020-05-28 11:49:57', '2020-05-28 11:49:57');

-- --------------------------------------------------------

--
-- Структура таблицы `cars__autoparks`
--

CREATE TABLE `cars__autoparks` (
  `car_id` int(11) NOT NULL,
  `autopark_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cars__autoparks`
--

INSERT INTO `cars__autoparks` (`car_id`, `autopark_id`) VALUES
(3, 2),
(5, 2),
(3, 3),
(7, 3),
(6, 4),
(9, 4),
(7, 4),
(6, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_05_26_185220_create_autoparks_table', 1),
(4, '2020_05_26_185258_create_cars_table', 2),
(5, '2020_05_26_193035_create_cars__autoparks_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'test@test.com', NULL, '$2y$10$ZqCsEqzYUjNlZtB4DksLzeeONvG01kiKtcqBafp5FdwgvCNavzYEW', 'driver', NULL, '2020-05-27 08:28:25', '2020-05-27 08:28:25'),
(2, 'testManager', 'manager@gmail.com', NULL, '$2y$10$LCY7GuAv3dwLRz74SBpGhuxg8/uTGE8vNJL2tCi4yedrCjR0OOA9i', 'manager', NULL, '2020-05-27 08:56:44', '2020-05-27 08:56:44'),
(3, 'testDriver', 'driver@test.com', NULL, '$2y$10$4RHKo.S4FB1MkRoemWkS8O1Exz91ZkS/8XnWbZKwO9aHTD//Y/w3u', 'driver', NULL, '2020-05-27 08:57:30', '2020-05-27 08:57:30'),
(4, 'driver2', 'd2@test.com', NULL, '$2y$10$Bl4aGX821pIUSmegzh/cYeWRuAq9aEm3s5GVdRqGrZg9wjvBV6Fje', 'driver', NULL, '2020-05-27 11:50:05', '2020-05-27 11:50:05');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `autoparks`
--
ALTER TABLE `autoparks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `autoparks`
--
ALTER TABLE `autoparks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `cars`
--
ALTER TABLE `cars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
