@extends('layouts.stdlayout')

@section('custom_style')

@endsection

@section('content')
    <div class="container">
        @include('errors')
        <form action="{{url('update_autopark')}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{$autopark->id}}">
            <div class="form-group">
                <label for="name">Название автопарка</label>
                <input name="name" type="text" id="name" class="form-control" value="{{$autopark->name}}" disabled>
            </div>
            <div class="form-group">
                <label for="address">Адресс</label>
                <input name="address" type="text" id="address" class="form-control" value="{{$autopark->address}}">
            </div>
            <div class="form-group">
                <label for="schedule">Расписание</label>
                <input name="schedule" type="text" id="schedule" class="form-control" value="<?= $autopark->schedule != null ? $autopark->schedule : '' ?>">
            </div>
            <button type="submit" class="btn btn-success">Edit</button>
        </form>
    </div>
@endsection
