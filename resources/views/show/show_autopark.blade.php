@extends('layouts.stdlayout')

@section('custom_style')

@endsection

@section('content')
    <div class="container">
        <h1>Автопарк {{$autopark->name}}</h1>
        @if (count($cars) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">номер</th>
                        <th scope="col">имя водителя</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($cars as $car)
                    <tr>
                        <th scope="row">{{$car->id}}</th>
                        <td>{{$car->car_number}}</td>
                        <td>{{$car->driver_id}}</td>
                        <td>
                            <form action="{{url('delete_car')}}" method="POST">
                                @csrf
                                {{method_field('POST')}}
                                <input type="hidden" name="car_id" value="{{$car->id}}">
                                <input type="hidden" name="autopark_id" value="{{$autopark->id}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        @include('errors')
        <button id="add-btn" class="btn btn-success">Добавить машину</button>
        <form id="add-form" style="display: none;" action="{{url('add_car')}}" method="POST">
            @csrf
            <div class="form-group">
                <input type="hidden" value="{{$autopark->id}}" name="autopark_id">
                <label for="car">Выберите машину</label>
                <select class="form-control" id="car" name="car_id">
                    @foreach($otherCars as $otherCar)
                        <option value="{{$otherCar->id}}">
                            {{$otherCar->car_number}}, {{$otherCar->driver_id}}.
                        </option>
                    @endforeach
                </select>
            </div>
            <button id="add-sbmt" type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
   <script>
        $('#add-btn').click(function () {
            $(this).css('display', 'none');
            $('#add-form').css('display', 'block');
        });

        $('$add-sbmt').click(function () {
            $('#add-form').css('display', 'none');
            $('#add-btn').css('display', 'block');
        });
    </script>
@endsection
