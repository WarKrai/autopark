@extends('layouts.stdlayout')

@section('custom_style')

@endsection

@section('content')
    <div class="container">
        <h1>Автопарки машины {{$car->car_number}}</h1>
        @if(count($autoparks) > 0)
            <ul class="list-group">
                @foreach($autoparks as $autopark)
                    <li class="list-group-item">{{$autopark->address}}, {{$autopark->address}}</li>
                @endforeach
            </ul>
        @else
            <p>У машины {{$car->car_number}} нет автопарков</p>
        @endif
    </div>
@endsection
