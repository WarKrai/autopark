@extends('layouts.stdlayout')

@section('custom_style')

@endsection

@section('content')
    <div class="container">
        @include('errors')
        <form action="{{url('car')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Номер машины</label>
                <input name="car_number" type="text" id="car_number" class="form-control">
            </div>
            <input type="hidden" name="driver_id" value="{{Auth::user()->id}}">
            <button type="submit" class="btn btn-success">Создать</button>
        </form>
    </div>
@endsection
