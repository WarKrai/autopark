@extends('layouts.stdlayout')

@section('custom_style')

@endsection

@section('content')
    @include('errors')
    <div class="container">
        <form action="{{url('autopark')}}" method="POST">
            @csrf
            {{method_field('POST')}}
            <div class="form-group">
                <label for="name">Название автопарка</label>
                <input type="text" id="name" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="address">Адресс</label>
                <input type="text" id="address" class="form-control" name="address">
            </div>
            <div class="form-group">
                <label for="schedule">Расписание</label>
                <input type="text" id="schedule" class="form-control" name="schedule">
            </div>
            <button type="submit" class="btn btn-success">Создать</button>
        </form>
    </div>
@endsection
