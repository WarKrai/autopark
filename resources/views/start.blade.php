@extends('layouts.stdlayout')

@section('custom_style')
    <style>
        .auth{
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%);
        }

        .welcome_title {
            position: absolute;
            top: 15%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%);
        }

        .table {
            position: absolute !important;
            top: 25% !important;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        @if (Route::has('login'))
            @auth
                <h1 class="welcome_title">Welcome, {{Auth::user()->name}}!   <button id="logout" class="btn btn-danger">{{__('Logout')}}</button></h1>


                <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                    @csrf
                </form>

                @if (Auth::user()->role == 'manager')
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Адресс</th>
                                <th scope="col">Расписание</th>
                                <th scope="col">Show</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($autoparks as $autopark)
                            <tr>
                                <th scope="row">{{$autopark->id}}</th>
                                <td>{{$autopark->name}}</td>
                                <td>{{$autopark->address}}</td>
                                @if (isset($autopark->schedule))
                                    <td>{{$autopark->schedule}}</td>
                                @else
                                    <td>-</td>
                                @endif
                                <td>
                                    <form action="{{url('show_autopark/'.$autopark->id)}}">
                                        @csrf
                                        <button type="submit" class="btn btn-dark">Show</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{url('edit_autopark/'.$autopark->id)}}" method="POST">
                                        @csrf
                                        <button class="btn btn-primary">Edit</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{url('autopark/'.$autopark->id)}}" method="POST">
                                        @csrf
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><button id="create_autopark" class="btn btn-success">Создать автопарк</button></td>
                        </tr>
                        </tbody>
                    </table>
                @elseif(Auth::user()->role == 'driver')
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Номер</th>
                            <th scope="col">Show</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($cars as $car)
                            <tr>
                                <th scope="row">{{$car->id}}</th>
                                <td>{{$car->car_number}}</td>
                                <td>
                                    <form action="{{url('/show_car/'.$car->id)}}" method="POST">
                                        @csrf
                                        {{method_field('POST')}}
                                        <button type="submit" class="btn btn-dark">Show</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><button id="create_car" class="btn btn-success">Добавить машину</button></td>
                        </tr>
                        </tbody>
                    </table>
                @endif
            @else
                <div class="auth">
                    <div>
                        <button id="login" class="btn btn-success">{{__('Login')}}</button>
                        @if (Route::has('register'))
                            <button id="register" class="btn btn-success">{{__("Register")}}</button>
                        @endif
                    </div>
                </div>
            @endauth
        @endif
    </div>


    <script>
        $('#login').click(function () {
            window.location.href='{{route('login')}}';
        });

        $('#register').click(function () {
           window.location.href='{{route('register')}}';
        });

        $('#logout').click(function () {
            event.preventDefault();
            document.getElementById('logout-form').submit();
            {{--window.location.href='{{route('logout')}}'--}}
        });

        $('#create_autopark').click(function () {
            window.location.href='/create_autopark';
        });

        $('#create_car').click(function () {
            window.location.href='/create_car';
        });
    </script>
@endsection
